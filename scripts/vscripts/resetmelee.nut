::MeleeModifiy <- {
	Events = {}
}

::MeleeModifiy.Events.OnGameEvent_round_start_post_nav <- function(event) {
	if (!("BotAI" in getroottable()))
		return;

	Msg("[Melee Reset]	检测到AI模组，执行近战数量修改\n");
	::BotAI.resetBotMeleeAction <- function() {
		local humanAmount = (BotAI.SurvivorList.len() - BotAI.SurvivorBotList.len());
		local teamAIsAmount = BotAI.SurvivorBotList.len();
		local survivorAmount = BotAI.SurvivorList.len();
		local meleeAmount = teamAIsAmount/2;
		/*if(survivorAmount <= 4)
			meleeAmount = BotAI.SurvivorList.len() - 1;
		else if(teamAIsAmount%2 != 0) {//odd
			if(teamAIsAmount == 1)
				meleeAmount = humanAmount + ((teamAIsAmount-1)/2) - 1;
			else
				meleeAmount = humanAmount + ((teamAIsAmount-1)/2);
		}
		else if(teamAIsAmount%2 == 0) {//even
			if(humanAmount == 1)
				meleeAmount = humanAmount + (teamAIsAmount/2);
			else if(humanAmount == 2)
				meleeAmount = (humanAmount + teamAIsAmount)/2;
			else if(humanAmount == 3)
				meleeAmount = (humanAmount + teamAIsAmount + 1)/2;
			else if(humanAmount == 4)
				meleeAmount = (humanAmount + teamAIsAmount)/2 + 1;
			else if(humanAmount == 5)
				meleeAmount = (humanAmount + teamAIsAmount + 3)/2;
			else if(humanAmount == 6)
				meleeAmount = (humanAmount + teamAIsAmount)/2 + 2;
			else
				meleeAmount = survivorAmount/2;
		}
		else
			meleeAmount = survivorAmount/2; */

		Convars.SetValue( "sb_max_team_melee_weapons", survivorAmount);
	}
}

__CollectEventCallbacks(::MeleeModifiy.Events, "OnGameEvent_", "GameEventCallbacks", RegisterScriptGameEventListener);
__CollectEventCallbacks(::MeleeModifiy.Events, "OnScriptEvent_", "ScriptEventCallbacks", RegisterScriptGameEventListener);